package com.example.uvatour;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class HistoryScreen extends Activity {
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history_screen);
		
		Button TourButton = (Button) findViewById(R.id.button4);
		Button MenuButton = (Button) findViewById(R.id.button2);
		Button TestButton = (Button) findViewById(R.id.button3);
		
		TourButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(HistoryScreen.this, MainActivity.class));
				finish();
			}
		});
		
		MenuButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(HistoryScreen.this, MenuScreen.class));
				finish();
			}
		});
		
		TestButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(HistoryScreen.this, TestScreen.class));
				finish();
			}
		});
		
		ListView list = (ListView) findViewById(R.id.listView1);

		
		
	}
		

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.history_screen, menu);
		return true;
	}

	public void onBackPressed() {
		android.app.AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Do you want to quit?")
			.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					HistoryScreen.this.finish();
				}
			}).setPositiveButton("No", null).show();
	}
}

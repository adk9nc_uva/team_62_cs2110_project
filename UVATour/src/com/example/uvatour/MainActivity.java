package com.example.uvatour;

import java.util.ArrayList;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {



	private int stopsMade = 0;
	private int numStops = 0;
	private GoogleMap map;
	private ArrayList<Location> tour;



	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button HistoryButton = (Button) findViewById(R.id.button1);
		Button MenuButton = (Button) findViewById(R.id.button2);
		Button TestButton = (Button) findViewById(R.id.button3);
		
		HistoryButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(MainActivity.this, HistoryScreen.class));
				finish();
			
			}
		});

		MenuButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(MainActivity.this, MenuScreen.class));
				finish();
			}
		});

		TestButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(MainActivity.this, TestScreen.class));
				finish();
			}
		});
		FragmentManager mang = getFragmentManager();
		MapFragment frag = (MapFragment)  mang.findFragmentByTag("map");
		GoogleMap map = frag.getMap();
		map.setMyLocationEnabled(true);
		MapListener list = new MapListener();
		map.setOnMyLocationChangeListener(list);
		
		
			
		}
	public class MapListener implements OnMyLocationChangeListener {

		@Override
		public void onMyLocationChange(Location loc) {
			Location newLoc = map.getMyLocation();
			Location dest = tour.get(1);
			LocationComparator locComp = new LocationComparator(loc, newLoc, dest);
			int comp = locComp.compare();
			
			
		}
		
	}



	


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onBackPressed() {
		android.app.AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Do you want to quit?")
		.setNegativeButton("Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				MainActivity.this.finish();
			}
		}).setPositiveButton("No", null).show();
	}

	public int getStopsMade() {
		return stopsMade;
	}


	public void setStopsMade(int stopsMade) {
		this.stopsMade = stopsMade;
	}


	public int getNumStops() {
		return numStops;
	}


	public void setNumStops(int numStops) {
		this.numStops = numStops;
	}
}
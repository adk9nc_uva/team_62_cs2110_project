package com.example.uvatour;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class TestScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_screen);
		Button HistoryButton = (Button) findViewById(R.id.button1);
		Button MenuButton = (Button) findViewById(R.id.button2);
		Button TourButton = (Button) findViewById(R.id.button4);
		EditText et1 = (EditText) findViewById(R.id.editText1);
		EditText et2 = (EditText) findViewById(R.id.editText2);
		
		Button BeginNew = (Button) findViewById(R.id.button5);
		BeginNew.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
		HistoryButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(TestScreen.this, HistoryScreen.class));
				finish();
			}
		});
		
		MenuButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(TestScreen.this, MenuScreen.class));
				finish();
			}
		});
		
		TourButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(TestScreen.this, MainActivity.class));
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test_screen, menu);
		return true;
	}
	
	public void onBackPressed() {
		android.app.AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Do you want to quit?")
			.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					TestScreen.this.finish();
				}
			}).setPositiveButton("No", null).show();
	}

}

package com.example.uvatour;

import sofia.app.Screen;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class SplashScreen extends Screen {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(4000);
                } catch (Exception e) {

                } finally {

                    startActivity(new Intent(SplashScreen.this,
                            StartScreen.class));
                    finish();
                }
            };
                 };
        splashTread.start();
        TextView tv1 = (TextView) findViewById(R.id.textView1);
        tv1.setTextColor(Color.WHITE);
        TextView tv2 = (TextView) findViewById(R.id.textView2);
        tv2.setTextColor(Color.WHITE);
        TextView tv3 = (TextView) findViewById(R.id.textView3);
        tv3.setTextColor(Color.WHITE);
        TextView tv4 = (TextView) findViewById(R.id.textView4);
        tv4.setTextColor(Color.WHITE);
        TextView tv5 = (TextView) findViewById(R.id.textView5);
        tv5.setTextColor(Color.WHITE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start_screen, menu);
		return true;
	}



}

package com.example.uvatour;

import android.location.Location;


public class LocationComparator {
	private Location old;
	private Location newL;
	private Location dest;
	public LocationComparator(Location oldL, Location newer, Location des){
		old=oldL;
		newL=newer;
		dest = des;
		
		
	}
	
	public int compare(){
		float disold = old.distanceTo(dest);
		float disnew = newL.distanceTo(dest);
		if(disold==disnew){
			return 0;
		}
		if(disold>disnew){
			return 1;
		}
		if(disold<disnew){
			return -1;
		}
		else{
			return 0;
		}
		
	}

}

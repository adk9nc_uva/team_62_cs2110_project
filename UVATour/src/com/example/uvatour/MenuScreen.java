package com.example.uvatour;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MenuScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_screen);
		Button HistoryButton = (Button) findViewById(R.id.button1);
		Button TourButton = (Button) findViewById(R.id.button4);
		Button TestButton = (Button) findViewById(R.id.button3);
		
		HistoryButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(MenuScreen.this, HistoryScreen.class));
				finish();
			}
		});
		
		TourButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(MenuScreen.this, MainActivity.class));
				finish();
			}
		});
		
		TestButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View button) {
				startActivity(new Intent(MenuScreen.this, TestScreen.class));
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_screen, menu);
		return true;
	}
	
	public void onBackPressed() {
		android.app.AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Do you want to quit?")
			.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					MenuScreen.this.finish();
				}
			}).setPositiveButton("No", null).show();
	}

}
